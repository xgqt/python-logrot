#!/usr/bin/env python3


""""
""" """
This file is part of python-logrot.

python-logrot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

python-logrot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with python-logrot.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2019-2022, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License
"""


import sys
import os


def main():
    """Main."""

    lib_path = os.path.join(
        os.path.dirname(__file__), "..", "src", "logrot_tool")
    sys.path.insert(0, lib_path)

    try:
        from logrot import main as lib_main
    except ImportError as exception:
        raise RuntimeError from exception

    lib_main.main()


if __name__ == "__main__":
    main()
