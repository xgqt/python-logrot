# This file is part of python-logrot.

# python-logrot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# python-logrot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with python-logrot.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2019-2022, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License


PYTHON          := python3
PIP             := pip
FIND            := find
MKDIR           := mkdir -p
RM              := rm -r
RMDIR           := $(RM) -d

SRCDIR          := $(PWD)/src


build-%:
	cd $(SRCDIR)/$(*) && $(PYTHON) $(SRCDIR)/$(*)/setup.py build

.PHONY: build
build: build-logrot-tool


bdist-%:
	cd $(SRCDIR)/$(*) && $(PYTHON) $(SRCDIR)/$(*)/setup.py bdist

sdist-%:
	cd $(SRCDIR)/$(*) && $(PYTHON) $(SRCDIR)/$(*)/setup.py sdist

wheel-%:
	cd $(SRCDIR)/$(*) && $(PYTHON) $(SRCDIR)/$(*)/setup.py bdist_wheel

dist-%:
	$(MAKE) sdist-$(*) wheel-$(*)

.PHONY: dist
dist: dist-logrot-tool

.PHONY: pkg
pkg: clean build dist


pip-install-%:
	$(PIP) install --user -e $(SRCDIR)/$(*)

install: pip-install-logrot-tool


pip-remove-%:
	$(PIP) uninstall --yes $(*)

remove: pip-remove-logrot-tool


clean-%:
	$(FIND) $(SRCDIR) -name $(*) -exec $(RMDIR) {} +

.PHONY: clean-cache
clean-cache:
	$(MAKE) clean-__pycache__
	$(MAKE) clean-build
	$(MAKE) clean-dist
	$(MAKE) clean-logrot_tool.egg-info

clean: clean-cache
