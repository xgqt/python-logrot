# LogRot


## About

Log rotation tool in Python

LogRot was moved from
[MyDot](https://gitlab.com/xgqt/mydot/)
to it's own repository in 2022 via the
[commit](https://gitlab.com/xgqt/mydot/-/commit/164d53f85ae09d02da16b9ea8b2d2f3aa575152c).
