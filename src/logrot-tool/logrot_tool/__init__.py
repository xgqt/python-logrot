#!/usr/bin/env python3


"""
""" """
This file is part of python-logrot.

python-logrot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

python-logrot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with python-logrot.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2019-2022, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License
"""


__version__ = "1.0.1"
